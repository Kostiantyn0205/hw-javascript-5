/*1)Це функція всередині об'єкта, яку можна використовувати для виконання операцій з властивостями об'єкта.*/
/*2)Властивість об'єкта може мати значення будь-якого типу даних.*/
/*3)Змінна, яка містить об'єкт, не містить самі дані об'єкта, а лише посилання на місце, де вони зберігаються*/

const createNewUser = function () {
    const newUser = {
        _firstName: "",
        _lastName: "",
        get login() {
            return `${this._firstName[0].toLowerCase()}${this._lastName.toLowerCase()}`
        },
        set firstName(newName) {
            this._firstName = newName;
        },
        set lastName(newName) {
            this._lastName = newName;
        }
    }
    newUser.firstName = prompt('Введіть своє ім\'я:');
    newUser.lastName = prompt('Введіть своє прізвище:');

    return newUser;
}
const user = createNewUser();
console.log(user.login);